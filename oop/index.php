<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');


$hewan = new animal("shaun");

echo "Name : " . $hewan->name . "<br>";
echo "legs : " . $hewan->legs . "<br>";
echo "cold blooded : " . $hewan->cold_blooded . "<br> <br>";

$kodok = new frog("buduk");

echo "Name : " . $kodok->name . "<br>";
echo "legs : " . $kodok->legs . "<br>";
echo "cold blooded : " . $kodok->cold_blooded . "<br>";
echo "jump : " . $kodok->jump();
echo "<br>";

$kera = new ape("kera sakti");

echo "Name : " . $kera->name . "<br>";
echo "legs : " . $kera->legs . "<br>";
echo "cold blooded : " . $kera->cold_blooded . "<br>";
echo "yell : " . $kera->yell();


?>