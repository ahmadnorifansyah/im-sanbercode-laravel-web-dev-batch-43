<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>
<body>
    <h1>Berlatih Looping</h1>

    <?php 
        echo "<h3>Soal No 1 Looping I Love PHP</h3>";

        echo "<h4>Looping 1</h4>";
        for($i = 2; $i<=20; $i+=2){
            echo $i . " - I Love PHP <br>";
        }

        echo "<h4>Looping 2</h4>";
        $w = 20;
        do {
            echo "$w - I Love PHP <br>";
            $w-=2;
        } while ($w >= 1);
        


        echo "<h3>Soal No 2 Looping Array Modulo </h3>";

        $numbers = [18, 45, 29, 61, 47, 34];
        echo "array numbers: ";
        print_r($numbers);
        echo "<br>";
        foreach($numbers as $value){
            $rest[] = $value%5;
        }

        echo "<br>";
        echo "Array sisa baginya adalah:  "; 
        print_r($rest);
        echo "<br>";

        echo "<h3> Soal No 3 Looping Asociative Array </h3>";

        $items = [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
        ];
        
        foreach($items as $arrayindex){
            $jualan = [
                'id' => $arrayindex[0],
                'barang' => $arrayindex[1],
                'harga' => $arrayindex[2],
                'deskripsi' => $arrayindex[3],
                'namafile' => $arrayindex[4]
            ];
            print_r($jualan);
            echo "<br>";
        } 
        
        echo "<h3>Soal No 4 Asterix </h3>";
            $asterix=5;
            for($k=$asterix; $k>0; $k--){
            for($l=$asterix; $l>=$k; $l--){
                echo " * ";
            }
            echo "<br>";
        }
               
    ?>

</body>
</html>