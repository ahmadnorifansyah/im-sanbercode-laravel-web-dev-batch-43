<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);
Route::get('/signin', [AuthController::class, 'form']);
Route::post('/welcome', [AuthController::class, 'send']);
Route::get('/table', function () {
    return view('page.table');
});
Route::get('/data-table', function () {
    return view('page.data-table');
});

//create data untuk direct ke halaman input data cast
route::get('/cast/create', [CastController::class, 'create']);
// rute untuk memasukan inputan ke db
Route::post('/cast', [CastController::class, 'store']);

//read data
//rute untuk menampilkan data yg telah di input
route::get('/cast', [CastController::class, 'index']);
route::get('/cast/{id}', [CastController::class, 'show']);

//Update datatatataarrrrrrrrrrrrrggh
//rute untuk mengarah ke form update data
route::get('/cast/{id}/edit', [CastController::class, 'edit']);
route::put('/cast/{id}', [CastController::class, 'update']);

//Delete Data
route::delete('/cast/{id}', [CastController::class, 'destroy']);

