<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.tambah');
    }

    public function store(Request $request)
    {
        // error validasi
        $request->validate([
            'name' => 'required',
            'age' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast')->insert([
            'nama' => $request['name'],
            'umur' => $request['age'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
 
        return view('cast.tampil', ['cast' => $cast]);
    }

    public function show($id)
    {
        $detail = DB::table('cast')->find($id);
        return view('cast/detail',['detail' => $detail]);
    }

    public function edit($id)
    {
        $edit = DB::table('cast')->find($id);
        return view('cast/edit',['edit' => $edit]);
    }

    public function update(Request $request, $id)
    {
         // error validasi
         $request->validate([
            'name' => 'required',
            'age' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                    'nama' => $request['name'],
                    'umur' => $request['age'],
                    'bio' => $request['bio']
                ]
            );

            return redirect('/cast');
    }

    public function destroy($id)
    {
        $deleted = DB::table('cast')->where('id', '=', $id)->delete();

        return redirect('/cast');
    }

}
