<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view('form.signin');
    }

    public function send(Request $request)
    {
        // dd($request);
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];

        return view('form.home', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
