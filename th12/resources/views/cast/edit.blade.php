@extends('layouts.master')
@section('title')
	Halaman Tambah Cast
@endsection

@section('sub-title')
	Input Data Cast
@endsection

@section('content')
<form action="/cast/{{$edit->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Cast</label>
      <input type="text" name="name" value="{{$edit->nama}}" class="form-control">
    </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur Cast</label>
      <input type="text" name="age" value="{{$edit->umur}}" class="form-control">
    </div>
    @error('age')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class="form-control" cols="30" rows="10">{{$edit->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection