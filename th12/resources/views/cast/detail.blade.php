@extends('layouts.master')
@section('title')
	Halaman Tampil Data Detail Dari Cast
@endsection

@section('sub-title')
    Berikut Detail Dari Cast :
@endsection

@section('content')
<h1>{{$detail->nama}}</h1>
<p>{{$detail->bio}}</p>
@endsection