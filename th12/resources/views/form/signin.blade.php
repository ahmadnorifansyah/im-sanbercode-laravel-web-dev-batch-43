@extends('layouts.master')

@section('title')
Buat Akun Baru!
@endsection

@section('sub-title')
Sign Up Form
@endsection

@section('content')
	<form action="/welcome" method="post">
		@csrf
		<label>First Name :</label> <br> <br>
			<input type="text" name="fname"> <br> <br>
		<label>Last Name :</label> <br> <br>
			<input type="text" name="lname"> <br> <br>
		<label>Gender :</label> <br> <br>
			<input type="radio" name="gender" value="1">Male<br>
			<input type="radio" name="gender" value="2">Female<br>
			<input type="radio" name="gender" value="3">Other<br> <br>
		<label>Nationality :</label> <br> <br>
			<select name="nationality">
				<option value="1">Indonesian</option>
				<option value="2">Malaysian</option>
				<option value="3">Korean</option>
				<option value="4">Singaporean</option>
				<option value="5">American</option>
			</select> <br> <br>
		<label>Languange Spoken :</label> <br> <br>
			<input type="checkbox" name="languange" value="1">Bahasa Indonesia<br>
			<input type="checkbox" name="languange" value="2">English<br>
			<input type="checkbox" name="languange" value="3">Others<br> <br>
		<label>Bio :</label> <br> <br>
		<textarea name="bio" rows="10" cols="30"></textarea> <br> <br>
		<input type="submit" value="kirim">
	</form>
@endsection