@extends('layouts.master')
@section('title')
	Halaman Utama
@endsection

@section('sub-title')
	Home
@endsection

@section('content')
	<h1>SanberBook</h1>
	<h2>Social Media Developer Santai Berkualitas</h2>
	<p>Belajar dan Berbagi agar hidup semakin santai berkualitas</p>
	<h3>Benefit Join Di SanberBook</h3>
	<ul>
		<li>Mendapatkan motivasi dari sesama developer</li>
		<li>Sharing knowledge dari para mastah sanber</li>
		<li>Dibuat oleh calon web developer terbaik</li>
	</ul>
	<h3>Cara bergabung ke SanberBook</h3>
	<ol>
		<li>Mengunjungi website ini</li>
		<li>Mendaftar di : <a href="/signin">Form Sign Up</a></li>
		<li>Selesai !</li>
	</ol>
@endsection